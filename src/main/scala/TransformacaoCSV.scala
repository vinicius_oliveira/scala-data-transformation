
import java.nio.file.{Files, Paths}
import scala.io.Source
import scala.jdk.CollectionConverters.*

object TransformacaoCSV {

  def main(args: Array[String]): Unit = {

    val dados = carregarDados("valores.csv")

    val dadosFiltrados = dados.filter(d => d.investimento >= 100000 && d.ano > 2022)

    val dadosMapeados = dadosFiltrados.map(d => (d.empresa, d.pais))

    criarArquivoCSV("valores-saida.csv", dadosMapeados)

  }

  def carregarDados(caminhoArquivo: String): Seq[Dados] = {

    val linhas = Source.fromFile(caminhoArquivo).getLines().toList

    linhas.tail.map { linha =>
      val colunas = linha.split(",")
      Dados(colunas(0), colunas(1), colunas(2).toDouble, colunas(3).toInt)
    }

  }

  def criarArquivoCSV(caminhoArquivo: String, dados: Seq[(String, String)]) : Unit = {

    val linhas = dados.map { case (empresa, pais) =>
      s"$empresa,$pais"
    }

    Files.write(Paths.get(caminhoArquivo), linhas.asJava)

  }

}
